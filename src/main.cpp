#include <Arduino.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <PubSubClient.h>

const int ANALOG_0 = 0;
const int ANALOG_1 = 1;
const int LED_PIN = 8;

Ethernet WSA; // WSAStartup
EthernetClient ethClient;

const char* mqtt_server = "192.168.1.93";
const int mqttPort = 1883; 
String mainTopic = "lightCaptor";
String resistorValTopic = mainTopic + "/resistorVal";
String ledPowerTopic = mainTopic + "/LedPower";
String systemStateTopic = mainTopic + "/SystemState";

bool systemState;
int previousCaptorValue = 0;
bool increment = true;



PubSubClient client(ethClient);

String payloadToString(byte* payload, unsigned int length){
  char buffer[length];
  sprintf(buffer, "%.*s", length, payload);
  return String(buffer);
}


void switchSystemState(String state)
{
 if(state == "on")
  {
    systemState = true;
  }
  else
  {
    systemState = false;
    digitalWrite(LED_PIN, 0);
    Serial.println("systeme eteint");
  }
}

void actionCallback(char* topicChar, byte* payloadByte, unsigned int length){
  String topic = String(topicChar);
  String payload = payloadToString(payloadByte, length);

  Serial.println(payload.toFloat());
  if(topic.equals(ledPowerTopic) && systemState)
    {
      digitalWrite(LED_PIN, payload.toFloat());
    }
  else if (topic.equals(systemStateTopic)){
    switchSystemState(payload);
  }
  else{
    return;
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  client.setServer(mqtt_server, mqttPort);
  client.setCallback(actionCallback);
}

void subscribeAction(){
  if(!client.subscribe(ledPowerTopic.c_str())){
    Serial.print("can not subscribe to ledPower");
  }
  Serial.print("subscribe to LedPower");
}

void subscribeSystemState(){
  if(!client.subscribe(systemStateTopic.c_str())){
    Serial.print("can not subscribe to systemState");
  }
  Serial.print("subscribe to systemState");
}
  

void connectMqtt(){
  if(!client.connected()){
    client.connect("capteur arduino");
    subscribeAction();
    subscribeSystemState();
  }
  
}

void publishCaptor(int mockedVal){
  if(!client.connected()){
    Serial.print("pas co");
    return;
  }
  if(!client.publish(resistorValTopic.c_str(), String(mockedVal).c_str())){
    Serial.print("can't send");
  }
}

int mockValue() 
{
  if (previousCaptorValue > 1000) {
    increment = false;
  } else if(previousCaptorValue < 40) {
    increment = true;
  }
  
  if (increment) {
    previousCaptorValue = (previousCaptorValue + 20) % 1024 ;

  } else {
     previousCaptorValue = (previousCaptorValue - 20) % 1024 ;
  }
  return map(previousCaptorValue, 0, 1024, 0, 255);
}

void loop() {
  connectMqtt();
  client.loop();
  if(systemState){
    publishCaptor(mockValue());
    delay(5000);
  }
  
};